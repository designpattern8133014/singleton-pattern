package com.config;

import java.sql.*;

public class DBConnection {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:mem:testdb";

    //  Database credentials
    static final String USER = "";
    static final String PASS = "";

    private static DBConnection dbConnection = null; //lazy loading
    private Connection connection;

    private DBConnection() {
        initializeConnection();
    }

    public static DBConnection getInstance(){
        if ((dbConnection == null)) {
            synchronized (DBConnection.class) { // thread safe
                if (dbConnection == null) {
                    dbConnection = new DBConnection();
                }
            }
        }
        return dbConnection;
    }

    public Connection initializeConnection() {
        try{
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected to H2 embedded database.");

            String sql = "SELECT * FROM students";

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            int count = 0;

            while (resultSet.next()) {
                count++;

                int ID = resultSet.getInt("ID");
                String name = resultSet.getString("name");
                System.out.println("Student #" + count + ": " + ID + ", " + name);
            }
            connection.close();
        } catch(SQLException se) {
        //Handle errors for JDBC
        se.printStackTrace();
    } catch(Exception e) {
        //Handle errors for Class.forName
        e.printStackTrace();
    }
        return connection;
    }
}
